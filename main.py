# PyStalker
# Author : Atakan Argın
# Github : atakanargn
from twython import Twython

consumer_key = 'CONSUMER-KEY-HERE'
consumer_secret = 'CONSUMER-SECRET-HERE'
access_token = 'ACCESS-TOKEN-HERE'
access_token_secret = 'TOKEN-SECRET-HERE'

twitter = Twython(app_key=consumer_key,
		  app_secret=consumer_secret,
		  oauth_token=access_token,
		  oauth_token_secret=access_token_secret)

def searchTweets(word,count):
    reSearch = twitter.search(q=str(word), count=count)
    tweets = reSearch['statuses']
    tweetDict = {'text':[],'user':[],'date':[]}
    for tweet in tweets:
        tweetDict['text'].append(tweet['text'])
        tweetDict['user'].append(tweet['user']['name'] + " | @"+tweet['user']['screen_name'])
        tweetDict['date'].append(tweet['created_at'])
    return tweetDict
